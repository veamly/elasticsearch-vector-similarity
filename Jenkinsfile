#!/usr/bin/env groovy

def projectId = 'veamly-184600'
def devCluster = 'veamly-platform-dev'
def stagingCluster = 'veamly-platform-uat'
def prodCluster = 'veamly-platform-prd'
def devClusterNamespace = 'veamly-platform-dev'
def stagingClusterNamespace = 'veamly-platform-uat'
def prodClusterNamespace = 'veamly-platform-prd'
def zone = 'us-central1-a'
def region = 'us-central1'

// kubectl create clusterrolebinding jenkins-veamly-cluster-admin-binding --clusterrole=cluster-admin --user=jenkins@veamly-184600.iam.gserviceaccount.com
// kubectl create secret generic veamly-platform-account-key --from-file=./veamly-platform-account.json --namespace=veamly-platform-prd

podTemplate(label: 'jhipster-v5.0.1',
    containers: [
        containerTemplate(
            name: 'jhipster',
            image: 'us.gcr.io/jenkins-187906/build-images/jhipster:v5.0.1',
            ttyEnabled: true,
            command: 'cat'
        )
    ],
    volumes: [
        hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock'),
        persistentVolumeClaim(mountPath: '/root/.m2/repository', claimName: 'maven-repo', readOnly: false),
        persistentVolumeClaim(mountPath: '/usr/local/share/.cache/yarn/v1', claimName: 'yarn-repo', readOnly: false)
    ]
) {
    node('jhipster-v5.0.1') {
        withCredentials([
            file(credentialsId: 'jenkins-veamly-file', variable: 'GOOGLE_APPLICATION_CREDENTIALS')
        ]) {
            container('jhipster') {
                sh "gcloud config set project ${projectId}"

                stage('checkout') {
                    def scmVars = checkout scm
                    commitHash = scmVars.GIT_COMMIT.take(10)
                }

                stage('test') {
                    try {
                        sh "./mvnw test"
                    } catch (err) {
                        throw err
                    } finally {
                        junit '**/target/surefire-reports/TEST-*.xml'
                    }
                }

                stage('package') {
                    sh "./mvnw package"
                }
                stage('Push image') {
                    sh 'gcloud auth activate-service-account --key-file=$GOOGLE_APPLICATION_CREDENTIALS'
                    sh 'gsutil cp target/releases/elasticsearch-binary-vector-scoring-5.6.0.zip gs://us.artifacts.veamly-184600.appspot.com/jars'
                }
            }
        }
    }
}
